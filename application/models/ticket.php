<?php

/**
 * The Ticket Model
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class Ticket extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_user_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_subject;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_department;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 1000
     * 
     */
    protected $_content;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 150
     * 
     */
    protected $_attachment;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_ticketID;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_ticket_status;

}
