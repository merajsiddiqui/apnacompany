<?php

/**
 * IFSC module to get bank details By IFSC code
 * @package  IFSC code
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */

namespace Bank;
use Curl\Http;
class IFSC
{
	private  $api_url = "https://ifsc.razorpay.com/";

	public  function getBankDetails($ifsc_code)
	{
		$request_uri = $this->api_url.$ifsc_code;
		$request = new Http();
		$request->get($request_uri);
		return $request->response->body;
	}
}



