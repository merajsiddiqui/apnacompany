<?php

namespace SendGrid;

use Framework\Registry;
class Mailer
{
	protected $from;
	protected $to;
	protected $bcc;
	protected $cc;
	protected $reply_to;
	protected $attachments;
	protected $body_text;
	protected $subject;
	private $sendgrid_api_key;

	public function __construct(){
		$configuration = Registry::get("configuration");
        $configuration = $configuration->initialize();
        $parsed = $configuration->parse("configuration/mail");
        $this->sendgrid_api_key  = $parsed->mail->default->sendgrid;
        if(!$this->sendgrid_api_key){
        	throw new \Exception("SendGrid API Not Found", 1);       	
        }	
	}
	/**
	 * From where the email will be sent
	 * @param array $sender_data details of sender
	 */
	public function addSender($sender_data)
	{
		$this->from = $sender_data;
	}

	/**
	 * Add list of reciever to specific mails
	 * @param array $receivers_list list of recipients
	 */
	public function addRecipient($receivers_list)
	{
		$this->to = $receivers_list;
	}

	/**
	 * Add list of reciever to specific mails as BCC
	 * @param array $bcc_list list of recipients
	 */
	public function addBCC($bcc_list)
	{
		$this->bcc = $bcc_list;
	}

	/**
	 * Add list of reciever to specific mails as CC
	 * @param array $cc_list list of recipients
	 */
	public function addCC($cc_list)
	{
		$this->cc = $cc_list;
	}

	/**	
	 * Add attachment to mail
	 * @param array $attachment_details attachment details like file name path etc.
	 */
	public function addAttachments($attachment_details)
	{
		$attachments = [];
		$this->header = ["Content-Type" => "application/json"];
		if(array_key_exists("name", $attachment_details)){
			$attachments[] = $this->buildAttachments($attachment_details);
		} else {
			foreach ($attachment_details as $single_attachment) {
				$attachments = array_merge($attachments,
								$this->buildAttachments($single_attachment));
			}
		}
		$this->attachments = $attachments;
	}

	public function buildAttachments($attachment) {
		if(strpos($attachment["name"], "/")!==false){
			$filename = end(explode("/", $attachment["name"]));
		} else {
			$filename = $attachment["name"];
		}
		return [
			"content" => base64_encode(file_get_contents($attachment["name"])),
			"disposition" => "attachment",
			"filename" => $filename,
			"name" => $filename,
			"type" => $attachment["type"]
		];
	}
	/**	
	 * Add subject for the mail 		
	 * @param string $subject subject of mail body
	 */
	public function addSubject($subject)
	{
		$this->subject = $subject;
	}

	/**
	 * Content for the mail body
	 * @param string $body_text html or plain code
	 */
	public function addBodyContent($body_text)
	{
		$this->body_text = $body_text;
	}

	public function addReplyTo($reply_to)
	{
		$this->reply_to = $reply_to;
	}
	public function send()
	{
		$sendgrid = new SendGrid($this->sendgrid_api_key);
		$request_body = $this->sendgridContentBuilder();
		$response = $sendgrid->client->mail()->send()->post($request_body);
		return $response->headers();
	}

	protected function sendgridContentBuilder()
	{
		$requst_data = [
			"content" => [[ "type" => "text/html",
							"value" => $this->body_text
						]],
			"from" => $this->from,
			"reply_to" => $this->reply_to,
			"subject" => $this->subject,
			"personalizations" => [[
								"to" => $this->to,
								]],
		];
		if($this->cc){
			$requst_data["personalizations"][0]["cc"] = $this->cc;
		}
		if($this->bcc){
			$requst_data["personalizations"][0]["cc"] = $this->bcc;
		}
		if($this->attachments){
			$requst_data["attachments"] = $this->attachments;
		}
		return $requst_data;
	}
}

