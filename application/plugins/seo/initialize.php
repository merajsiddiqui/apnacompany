<?php

// initialize seo
include("seo.php");

$seo = new SEO(array(
    "title" => "Omanzo",
    "keywords" => "Omanzo, shopping, ecommerce, earn by shopping, network marketing" ,
    "description" => "We are One of the best Ecommerce ",
    "author" => "Meraj Ahmad Siddiqui",
    "robots" => "INDEX,FOLLOW",
    "photo" => CDN . "images/logo.png"
));

Framework\Registry::set("seo", $seo);