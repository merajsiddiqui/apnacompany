<?php

class AccountStatement extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_user_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_report_content;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_transaction_amount;
}
