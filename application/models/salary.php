<?php

/**
 * The Salary Model
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */

class Salary extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_employee_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_salary_month;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_salary_amount;


}
