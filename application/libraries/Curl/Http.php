<?php

/**
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */

namespace Curl;

class Http {

	public $data;
	public $response;
	public $custom_headers;

	private $curl_options;

	public function __construct(){
		$this->curl_options = [
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_AUTOREFERER => true,
			CURLOPT_COOKIESESSION => true,
			CURLOPT_FILETIME => true,
			CURLOPT_FRESH_CONNECT => true,
		];

		$this->headers = [
			'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8;',
			'Accept-Language: en-US,en;q=0.5',
			'Accept-Encoding: gzip, deflate',
			"Connection: keep-alive",
			"Content-Type: text/html; charset=UTF-8",
		];
	}

	public function setHeaders($headers) {
		$this->headers = $headers;
	}

	public function setBody ($body_data){
		$this->data = $body_data;
	}

	public function get($uri){
		$this->curl_options[CURLOPT_URL] = $uri;
		$this->response = $this->request();
	}

	public function post($uri){
		$this->curl_options[CURLOPT_URL] = $uri;
		$this->curl_options[CURLOPT_POST]=true;
		$this->curl_options[CURLOPT_POSTFIELDS] = $this->data;
		$this->response = $this->request();

	}

	protected function request(){
		$ch = curl_init();
		curl_setopt_array($ch, $this->curl_options);
		$body = curl_exec($ch);
		$error = curl_getinfo($ch);
		$headers = curl_error($ch);
		curl_close($ch);
		$data = json_encode(["headers" => $headers, "error" => $error, "body"=>$body]);
		return json_decode($data);
	}
}
