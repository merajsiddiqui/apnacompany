<?php
/**
 * Auth COntroller to auuthenticate the user
 *
 * @author 
 */
use Shared\Controller;
use Framework\Registry;
use Framework\RequestMethods;
use Payment\Payumoney;
use PDF\Invoice;
use Bank\IFSC;
use Geneology\Users;
class Home extends Controller {

    public function index(){
        $layout = $this->getLayoutView();
        $layout->set("seo", Registry::get("seo"));
        $view = $this->getActionView();
    }

    public function about(){
    	$this->seo([
            "title" => "About Us",
            "keywords" => "about",
            "description" => "admin",
            "view" => $this->getLayoutView()
        ]);
    	$view = $this->getActionView();
    }

    public function contact(){
        $this->seo([
            "title" => "Contact Us",
            "keywords" => "contact",
            "description" => "admin",
            "view" => $this->getLayoutView()
        ]);
        $view = $this->getActionView();
    }

    public function services(){
        $this->seo([
            "title" => "Buy Your Favorite Brand",
            "keywords" => "contact",
            "description" => "admin",
            "view" => $this->getLayoutView()
        ]);
        $view = $this->getActionView();
    }

    public function visitors(){
        $this->noview();
        $visitor = new Visitors\Visitor();
        $user_info = $visitor->getInfo();
        var_dump($user_info);
    }

}