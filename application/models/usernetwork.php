<?php

/**
 * The UserNetwork Model
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class UserNetwork extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 25
     * 
     */
    protected $_user_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 25
     */
    protected $_referal_code;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_parent_user_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 2
     */
    protected $_position;
 
}
