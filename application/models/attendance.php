<?php

/**
 * The Attendance Model
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class Attendance extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_employee_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_login_time;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_logout_time;


}
