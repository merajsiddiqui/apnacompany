<?php

namespace Proactive;

use Framework\Registry;

class SMS
{

	protected $api_url = "http://sms.proactivesms.in/sendsms.jsp";

	private $username;

	private $password;

	private $senderid;

	protected $recipients;

	protected $message;

	public function __construct() {
		$configuration = Registry::get("configuration");
        $configuration = $configuration->initialize();
        $parsed = $configuration->parse("configuration/sms");
        $this->username  = $parsed->proactive->username;
        $this->password  = $parsed->proactive->password;
        $this->senderid  = $parsed->proactive->senderid;
        if(!$this->username || !$this->password || !$this->senderid){
        	throw new \Exception("Proactive API keys not found", 1);       	
        }
	}

	public function addRecipients($mobile_number)
	{
		$this->recipients = $mobile_number; 
	}

	public function addMessage($message)
	{
		$this->message  = trim($message);
	}

	public function send()
	{
		$request_uri = $this->api_url."?".$this->buildMessageUri();
		$curl_handler = curl_init();
		$curl_options = [
			CURLOPT_URL => $request_uri,
			CURLOPT_HEADER => true,
			CURLOPT_RETURNTRANSFER => true
		];
		curl_setopt_array($curl_handler, $curl_options);
		$response = curl_exec($curl_handler);
		curl_close();
		return $response;
	}

	protected function buildMessageUri()
	{
		$uri = "user=".$this->username;
		$uri .= "&password=".$this->password;
		$mobile = '';
		if(is_array($this->recipients) && count($this->recipients) < 300){
			foreach ($this->recipients as $mobile_number) {
				$mobiles .= $mobile_number.",";	
			}
			$mobiles = rtrim($mobiles, ",");
		} else if(is_array($this->recipients) && count($this->recipients)) {
			throw new \Exception("Maximum of 300 recipients are allowed", 1);
		}else {
			$mobiles = $this->recipients;
		}
		$uri .= "&mobiles=".$mobiles;
		$uri .= "&senderid=".$this->senderid;
		$uri .= "&sms=".$this->message; 
		return $uri;
	}
}
