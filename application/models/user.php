<?php

/**
 * The User Model
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class User extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     * @label name
     */
    protected $_name;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_email;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_phone;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 130
     * @index
     * 
     * @label password
     */
    protected $_password;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 150
     * 
     */
    protected $_access_token;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_access_expiry;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_user_type;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_dob;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_referer_id;  

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_address; 
}
